External repository
------------------

[gitlab.com](https://gitlab.com/Alex_Gens/sp-07)

System requirements
-------------------

Java Development Kit v.1.8.0_201

Maven 4.0.0

Software
-------
Java 8

Maven 4

PostgreSql

Developers
---------

name: Kazakov Alexey

email: aleks25000@gmail.com

Build application
-----------------
    mvn clean

    mvn install

Run application
-------------

    mvn springboot:run



