package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.model.Project;

import java.util.List;

public interface IProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    public List<Project> findAllByUserIdOrderByDateCreate(@NotNull final String userId);

    @Nullable
    public Project findByIdAndUserId(@NotNull final String id,
                                     @NotNull final String userId);

}
