package ru.kazakov.iteco.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.model.Role;

public interface IRoleRepository extends JpaRepository<Role, String> {}
