package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.model.User;
import java.util.List;

public interface IUserRepository extends JpaRepository<User, String> {

    public User findByUsername(@NotNull final String username);

    public List<User> findAllByOrderByDateCreate();

}
