package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.model.Project;

import java.util.List;

public interface IProjectService {

    public Project save(@Nullable final Project s) throws Exception;

    @Nullable
    public Project findById(@Nullable final String id) throws Exception;

    public Project findOne(@Nullable final String id,
                           @Nullable final String userId) throws Exception;

    public boolean existsById(@Nullable final String id) throws Exception;

    @NotNull
    public List<Project> findAllByUserIdOrderByDateCreate(@Nullable final String userId) throws Exception;

    public void deleteById(@Nullable final String id) throws Exception;

    public void delete(@Nullable final Project project) throws Exception;

}
