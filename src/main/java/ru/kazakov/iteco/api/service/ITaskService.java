package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.model.Task;

import java.util.List;

public interface ITaskService {

    public Task save(@Nullable final Task task) throws Exception;

    public Task findById(@Nullable final String id) throws Exception;

    public Task findOne(@Nullable final String id,
                        @Nullable final String userId
    ) throws Exception;

    @NotNull
    public List<Task> findAllByUserIdOrderByDateCreate(@Nullable final String userId) throws Exception;

    public void deleteById(@Nullable final String id) throws Exception;

    public void delete(@Nullable final Task task) throws Exception;

}
