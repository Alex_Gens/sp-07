package ru.kazakov.iteco.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.dto.TaskDTO;
import java.util.List;

@FeignClient("task")
public interface TaskClient {

    static TaskClient client(@NotNull final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @GetMapping("/tasks")
    List<TaskDTO> getAllTask(@RequestHeader("cookie") @NotNull final String header);

    @GetMapping(value = "/tasks/{id}")
    TaskDTO getTask(@RequestHeader("cookie") @NotNull final String header,
                    @PathVariable(value = "id") @Nullable final String id);

    @PostMapping("/tasks")
    public TaskDTO addTask(@RequestHeader("cookie") @NotNull final String header,
                           @Nullable final TaskDTO dto);

    @DeleteMapping("/tasks/{id}")
    public void deleteTask(@RequestHeader("cookie") @NotNull final String header,
                           @PathVariable(value = "id") @Nullable final String id);

    @PutMapping("/tasks")
    public void updateTask(@RequestHeader("cookie") @NotNull final String header,
                           @Nullable final TaskDTO dto);

}