package ru.kazakov.iteco.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.kazakov.iteco.dto.UserDTO;

@FeignClient("user")
public interface UserClient {

    static UserClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(UserClient.class, baseUrl);
    }

    @PostMapping(value = "/users/login_user", produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(@NotNull final UserDTO dto);


    @PostMapping(value = "/users/logout_user", produces = MediaType.APPLICATION_JSON_VALUE)
    public void logout(@RequestHeader("cookie") @NotNull final String header);

}
