package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.User;
import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IUserService userService;

    @RequestMapping("/all_projects")
    public String getProjects(@NotNull final Model model) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) return "redirect:";
        List<Project> projects = projectService.findAllByUserIdOrderByDateCreate(currentUser.getId());
        model.addAttribute("entityList", projects);
        return "project/project";
    }

    @GetMapping("/project_create")
    public String createProject(@NotNull final Model model) {
        @NotNull final Project project = new Project();
        model.addAttribute("project", project);
        model.addAttribute("values", Status.values());
        return "project/project_create";
    }

    @PostMapping(value = "/project_create")
    public String addNewProject(
           @ModelAttribute("project")
           @NotNull final Project project
    ) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        project.setUser(currentUser);
        projectService.save(project);
        return "redirect:/all_projects";
    }

    @GetMapping(value = "/project/{id}")
    public String viewProject(
            @NotNull final Model model,
            @PathVariable final String id
    ) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        @Nullable final Project project = projectService.findOne(id, currentUser.getId());
        if (project == null) return "/404";
        if (!project.getUser().getId().equals(currentUser.getId())) return "/404";
        @Nullable final ProjectDTO dto = domainService.getProjectDTO(project);
        model.addAttribute("project", dto);
        return "project/project_view";
    }

    @GetMapping(value = "/project_update/{id}")
    public String updateProject(
            @NotNull final Model model,
            @PathVariable final String id
    ) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        @Nullable final Project project = projectService.findOne(id, currentUser.getId());
        if (project == null) return "/404";
        if (!project.getUser().getId().equals(currentUser.getId())) return "/404";
        @Nullable final ProjectDTO dto = domainService.getProjectDTO(project);
        model.addAttribute("project", dto);
        model.addAttribute("values", Status.values());
        return "/project/project_update";
    }

    @PostMapping(value = "/project_update/{id}")
    public String mergeProject(
            @ModelAttribute final ProjectDTO dto
    ) throws Exception {
        if (dto == null) throw new Exception();
        @Nullable final Project project = domainService.getProjectFromDTO(dto);
        projectService.save(project);
        return "redirect:/all_projects";
    }

    @GetMapping(value = "/project_remove/{id}")
    public String removeProject(
            @PathVariable final String id
    ) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        @Nullable final Project project = projectService.findOne(id, currentUser.getId());
        if (project == null) return "/404";
        if (!project.getUser().getId().equals(currentUser.getId())) return "/404";
        projectService.deleteById(id);
        return "redirect:/all_projects";
    }

    @GetMapping("/project/redirect")
    public String redirect() {return "redirect:/all_projects";}

}
