package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.model.User;
import ru.kazakov.iteco.validator.UserValidator;
import java.util.List;

@Controller
@SessionAttributes()
public class UserController {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private UserValidator validator;

    @NotNull
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/login")
    public String login(@NotNull final Model model,
                        @Nullable final String error,
                        @Nullable final String logout
    ) {
        if (error != null) {
            model.addAttribute("error", "Username or password is incorrect.");
        }
        if (logout != null) {
            model.addAttribute("message", "Logged out successfully.");
        }
        @NotNull final String authStatus =
                SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        model.addAttribute("isAuth", !authStatus.equals("anonymousUser"));
        return "login/login";
    }

    @GetMapping("/registration")
    public String registration(@NotNull final Model model) {
        @NotNull final User user = new User();
        model.addAttribute("user", user);
        @NotNull final String authStatus =
                SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        model.addAttribute("isAuth", !authStatus.equals("anonymousUser"));
        return "login/registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") @Nullable final User user,
                               @NotNull final BindingResult bindingResult
    ) throws Exception {
        validator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) return "login/registration";
        if (user == null) return "login/registration";
        userService.create(user.getUsername(), user.getPassword());
        return "login/login";
    }

    @RequestMapping("/admin/all_users")
    public String getUsers(@NotNull final Model model) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) return "redirect:";
        List<User> users = userService.findAllByOrderByDateCreate();
        model.addAttribute("entityList", users);
        return "admin/user";
    }

    @GetMapping(value = "/admin/user_update/{id}")
    public String changePassword(
            @NotNull final Model model,
            @PathVariable String id) throws Exception {
        @Nullable final User user = userService.findById(id);
        @Nullable final UserDTO dto = domainService.getUserDTO(user);
        if (dto == null) throw new Exception();
        dto.setPasswordHash("");
        model.addAttribute(dto);
        return "admin/user_update";
    }

    @PostMapping(value = "/admin/user_update/{id}")
    public String changePassword(
            @ModelAttribute @Nullable final UserDTO dto
    ) throws Exception {
        if (dto == null) throw new Exception();
        if (dto.getPasswordHash() == null || dto.getPasswordHash().isEmpty()) return "redirect:/admin/user_update/{id}";
        @Nullable final User user = domainService.getUserFromDTO(dto);
        if (user == null) throw new Exception();
        user.setPassword(encoder.encode(dto.getPasswordHash()));
        userService.save(user);
        return "redirect:/admin/all_users";
    }

    @GetMapping(value = "/admin/user_remove/{id}")
    public String removeProject(
            @PathVariable final String id
    ) throws Exception {
        userService.deleteById(id);
        return "redirect:/admin/all_users";
    }

    @GetMapping("/admin/user/redirect")
    public String redirect() {return "redirect:/admin/all_users";}

}
