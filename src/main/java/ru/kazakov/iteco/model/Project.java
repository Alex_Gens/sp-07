package ru.kazakov.iteco.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
public class Project extends AbstractEntity {

    @Column
    @Nullable
    private String name;

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(name = "date_create")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateCreate = new Date();

    @Nullable
    @Column(name = "date_start")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateFinish;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Column
    @Nullable
    private String description;

    @Nullable
    @OneToMany(mappedBy = "project")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Task> tasks = new ArrayList<>();

}
