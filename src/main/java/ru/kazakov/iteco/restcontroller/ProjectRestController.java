package ru.kazakov.iteco.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.User;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/projects",
                produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class ProjectRestController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IUserService userService;

    @GetMapping
    public List<ProjectDTO> getAllProjects() throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        return projectService.findAllByUserIdOrderByDateCreate(currentUser.getId())
                .stream().map(v -> domainService.getProjectDTO(v)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ProjectDTO getProject(@PathVariable @Nullable final String id) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        @Nullable final Project project = projectService.findOne(id, currentUser.getId());
        if (project == null) return null;
        if (!project.getUser().getId().equals(currentUser.getId())) throw new Exception();
        return domainService.getProjectDTO(project);
    }

    @PostMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ProjectDTO> addProject(@RequestBody @Nullable final ProjectDTO dto) throws Exception {
        @Nullable final Project project = domainService.getProjectFromDTO(dto);
        if (project == null) return null;
        if (dto == null) return null;
        @NotNull final Project newProject = projectService.save(project);
        @NotNull final ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentContextPath();
        @NotNull final UriComponents uriComponents = builder.path("/projects/{id}").buildAndExpand(dto.getId());
        @NotNull final URI uri = uriComponents.toUri();
        @NotNull final ResponseEntity<ProjectDTO> responseEntity = ResponseEntity.created(uri).body(dto);
        return responseEntity;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable @Nullable final String id) throws Exception {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (currentUser == null) throw new Exception();
        @Nullable final Project project = projectService.findOne(id, currentUser.getId());
        if (project == null) throw new Exception();
        if (!project.getUser().getId().equals(currentUser.getId())) throw new Exception();
        projectService.deleteById(id);
    }

    @PutMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void updateProject(@RequestBody @Nullable final ProjectDTO dto) throws Exception {
        @Nullable final Project project = domainService.getProjectFromDTO(dto);
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final User currentUser = userService.findByUsername(currentUsername);
        if (project == null) throw new Exception();
        if (currentUser == null) throw new Exception();
        if (!project.getUser().getId().equals(currentUser.getId())) throw new Exception();
        projectService.save(project);
    }

}
