package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DomainService implements IDomainService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Nullable
    public ProjectDTO getProjectDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO dto = new ProjectDTO();
        dto.setId(project.getId());
        dto.setUserId(project.getUser().getId());
        dto.setName(project.getName());
        dto.setDateCreate(project.getDateCreate());
        dto.setDateStart(project.getDateStart());
        dto.setDateFinish(project.getDateFinish());
        dto.setStatus(project.getStatus());
        dto.setDescription(project.getDescription());
        return dto;
    }

    @Nullable
    public Project getProjectFromDTO(@Nullable final ProjectDTO dto) throws Exception {
        if (dto == null) return null;
        @NotNull final Project project = new Project();
        project.setId(dto.getId());
        if (dto.getUserId() == null || dto.getUserId().isEmpty()) throw new Exception();
        else {
            @Nullable final User user = userService.findById(dto.getUserId());
            if (user == null) throw new Exception();
            project.setUser(user);
        }
        project.setName(dto.getName());
        project.setDateCreate(dto.getDateCreate());
        project.setDateStart(dto.getDateStart());
        project.setDateFinish(dto.getDateFinish());
        project.setStatus(dto.getStatus());
        project.setDescription(dto.getDescription());
        return project;
    }

    @Nullable
    public TaskDTO getTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO dto = new TaskDTO();
        dto.setId(task.getId());
        dto.setUserId(task.getUser().getId());
        if (task.getProject() == null) {
            dto.setProjectId(null);
            dto.setProjectName(null);
        }
        else {
            dto.setProjectId(task.getProject().getId());
            dto.setProjectName(task.getProject().getName());
        }
        dto.setName(task.getName());
        dto.setDateCreate(task.getDateCreate());
        dto.setDateStart(task.getDateStart());
        dto.setDateFinish(task.getDateFinish());
        dto.setStatus(task.getStatus());
        dto.setDescription(task.getDescription());
        return dto;
    }

    @Nullable
    public Task getTaskFromDTO(@Nullable final TaskDTO dto) throws Exception {
        if (dto == null) return null;
        @NotNull final Task task = new Task();
        task.setId(dto.getId());
        if (dto.getUserId() == null || dto.getUserId().isEmpty()) throw new Exception();
        else {
            @Nullable final User user = userService.findById(dto.getUserId());
            if (user == null) throw new Exception();
            task.setUser(user);
        }
        if (dto.getProjectId() == null || dto.getProjectId().isEmpty()) task.setProject(null);
        else {
            @Nullable final Project project = projectService.findById(dto.getProjectId());
            task.setProject(project);
        }
        task.setName(dto.getName());
        task.setDateCreate(dto.getDateCreate());
        task.setDateStart(dto.getDateStart());
        task.setDateFinish(dto.getDateFinish());
        task.setStatus(dto.getStatus());
        task.setDescription(dto.getDescription());
        return task;
    }

    @Nullable
    public UserDTO getUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO dto = new UserDTO();
        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        List<RoleType> roles = user.getRoles().stream().map(Role::getRole).collect(Collectors.toList());
        dto.setRoles(roles);
        dto.setDateCreate(user.getDateCreate());
        return dto;
    }

    @Nullable
    public User getUserFromDTO(@Nullable final UserDTO dto) {
        if (dto == null) return null;
        @NotNull final User user = new User();
        user.setId(dto.getId());
        user.setUsername(dto.getUsername());
        user.setDateCreate(dto.getDateCreate());
        return user;
    }

}
