package ru.kazakov.iteco.client;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TaskClientTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    private final String testId = UUID.randomUUID().toString();

    @NotNull
    private final String testName = "Test";

    @NotNull
    private final String url = "http://localhost:8080/";

    @NotNull
    @Autowired
    private PasswordEncoder encoder;

    @Nullable
    private User testUser;

    @Nullable
    private User adminUser;

    @Nullable
    private UserDTO userDto;

    @Nullable
    private TaskDTO testDto;

    @Nullable
    private String sessionId;

    @Before
    public void init() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword(encoder.encode("pass"));
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
        if (userService.findByUsername("adminTest") == null) {
            @NotNull final User user = new User();
            user.setId("adminTestId");
            user.setUsername("adminTest");
            user.setPassword(encoder.encode("pass"));
            adminUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.ADMINISTRATOR);
            roleService.save(role);
        }
        userDto = new UserDTO();
        userDto.setId("userTestId");
        userDto.setUsername("userTest");
        userDto.setPasswordHash("pass");
        sessionId = UserClient.client(url).login(userDto);
        testDto = new TaskDTO();
        testDto.setDateCreate(new Date());
        testDto.setId(testId);
        testDto.setName(testName);
        testDto.setUserId(userDto.getId());
        TaskClient.client(url).addTask(sessionId, testDto);

    }

    @After
    public void termTestDto() throws Exception {
        @Nullable ProjectDTO dto;
        try {
            dto = ProjectClient.client(url).getProject(sessionId, testId);
        } catch (FeignException e) {
            userDto = new UserDTO();
            userDto.setId("userTestId");
            userDto.setUsername("userTest");
            userDto.setPasswordHash("pass");
            sessionId = UserClient.client(url).login(userDto);
            dto = ProjectClient.client(url).getProject(sessionId, testId);
        }
        if (dto != null) {
            ProjectClient.client(url).deleteProject(sessionId, testId);
        }
        testDto = null;
        UserClient.client(url).logout(sessionId);
        sessionId = null;
        userDto = null;
        Assert.assertNotNull(testUser);
        Assert.assertNotNull(adminUser);
        if (userService.findById(testUser.getId()) != null) userService.deleteById(testUser.getId());
        if (userService.findById(adminUser.getId()) != null) userService.deleteById(adminUser.getId());
    }

    @Test(expected = FeignException.class)
    public void secureAccess() {
        UserClient.client(url).logout(sessionId);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
    }

    @Test
    public void getAllTasks() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        test2.setUserId("adminTestId");
        TaskClient.client(url).addTask(sessionId, test2);
        @Nullable final List<TaskDTO> dtos = TaskClient.client(url).getAllTask(sessionId);
        Assert.assertNotNull(dtos);
        Assert.assertEquals(1, dtos.size());
        UserClient.client(url).logout(sessionId);
        @NotNull final UserDTO adminDto = new UserDTO();
        adminDto.setId("adminTestId");
        adminDto.setUsername("adminTest");
        adminDto.setPasswordHash("pass");
        @NotNull final String adminSessionId = UserClient.client(url).login(adminDto);
        TaskClient.client(url).deleteTask(adminSessionId, test2.getId());
        UserClient.client(url).logout(adminSessionId);
        sessionId = UserClient.client(url).login(userDto);
    }

    @Test
    public void getTask() {
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getId(), testId);
    }

    @Test
    public void addTask() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        Assert.assertNotNull(userDto);
        test2.setUserId(userDto.getId());
        TaskClient.client(url).addTask(sessionId, test2);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, test2.getId());
        Assert.assertNotNull(dto);
        Assert.assertEquals(test2.getId(), dto.getId());
        TaskClient.client(url).deleteTask(sessionId, test2.getId());
    }

    @Test
    public void deleteTask() {
        TaskClient.client(url).deleteTask(sessionId, testId);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNull(dto);
    }

    @Test
    public void updateTask() {
        Assert.assertNotNull(testDto);
        @NotNull final String updatedName = "TestUpdated";
        testDto.setName(updatedName);
        TaskClient.client(url).updateTask(sessionId, testDto);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getName(), updatedName);
    }

}
