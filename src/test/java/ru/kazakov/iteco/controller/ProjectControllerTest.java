package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ProjectControllerTest {

    @Nullable
    @Autowired
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Nullable
    private User testUser;

    @Before
    public void initMock() {
       mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Before
    public void initTestUser() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @Test
    public void getProjectsNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_projects"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass", roles = "USER")
    public void getProjects() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_projects"))
                .andExpect(status().isOk());
    }

    @Test
    public void createProjectNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project_create"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass", roles = "USER")
    public void createProject() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_projects"))
                .andExpect(status().isOk())
                .andExpect(view().name("project/project"));
    }

    @Test
    public void viewProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project/projectTestId"))
                .andExpect(status().isUnauthorized());
        projectService.delete(project);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void viewProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project/projectTestId"))
                .andExpect(status().isOk())
                .andExpect(view().name("project/project_view"));
        projectService.delete(project);
    }

    @Test
    public void updateProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project_update/projectTestId"))
                .andExpect(status().isUnauthorized());
        projectService.delete(project);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void updateProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project_update/projectTestId"))
                .andExpect(status().isOk())
                .andExpect((view().name("/project/project_update")));
        projectService.delete(project);
    }

    @Test
    public void deleteProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project_remove/projectTestId"))
                .andExpect(status().isUnauthorized());
        projectService.delete(project);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void deleteProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/project_remove/projectTestId"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/all_projects"));
        Assert.assertNull(projectService.findById(project.getId()));
    }

}
