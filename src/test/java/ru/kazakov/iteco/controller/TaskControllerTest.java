package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TaskControllerTest {

    @Nullable
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Nullable
    private User testUser;

    @Before
    public void initMock() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Before
    public void initTestUser() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @Test
    public void getTasksNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_tasks"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass", roles = "USER")
    public void getTasks() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_tasks"))
                .andExpect(status().isOk());
    }

    @Test
    public void createTaskNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task_create"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass", roles = "USER")
    public void createTask() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/all_tasks"))
                .andExpect(status().isOk())
                .andExpect(view().name("task/task"));
    }

    @Test
    public void viewTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task/taskTestId"))
                .andExpect(status().isUnauthorized());
        taskService.delete(task);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void viewTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task/taskTestId"))
                .andExpect(status().isOk())
                .andExpect(view().name("/task/task_view"));
        taskService.delete(task);
    }

    @Test
    public void updateTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task_update/taskTestId"))
                .andExpect(status().isUnauthorized());
        taskService.delete(task);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void updateTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task_update/taskTestId"))
                .andExpect(status().isOk())
                .andExpect((view().name("/task/task_update")));
        taskService.delete(task);
    }

    @Test
    public void deleteTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task_remove/taskTestId"))
                .andExpect(status().isUnauthorized());
        taskService.delete(task);
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void deleteTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/task_remove/taskTestId"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/all_tasks"));
        Assert.assertNull(taskService.findById(task.getId()));
    }

}