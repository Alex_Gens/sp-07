package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class UserControllerTest {

    @Nullable
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Nullable
    private User testUser;

    @Nullable
    private User adminUser;

    @Before
    public void initMock() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
        if (userService.findByUsername("adminTest") == null) {
            @NotNull final User user = new User();
            user.setId("adminTestId");
            user.setUsername("adminTest");
            user.setPassword("pass");
            adminUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.ADMINISTRATOR);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        Assert.assertNotNull(adminUser);
        if (userService.findById(testUser.getId()) != null) userService.deleteById(testUser.getId());
        if (userService.findById(adminUser.getId()) != null) userService.deleteById(adminUser.getId());
    }

    @Test
    public void login() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
               .andExpect(status().isOk())
               .andExpect(view().name("login/login"));
    }

    @Test
    public void registration() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("login/registration"));
    }

    @Test
    public void getUsersNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/all_users"))
               .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void getUsersNotAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/all_users"))
               .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "adminTest", password = "pass", roles = {"ADMINISTRATOR"})
    public void getUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/all_users"))
                .andExpect(status().isOk());
    }

    @Test
    public void changePasswordNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_update/userTestId"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void changePasswordNotAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_update/userTestId"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "adminTest", password = "pass", roles = {"ADMINISTRATOR"})
    public void changePassword() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_update/userTestId"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/user_update"));
    }

    @Test
    public void removeProjectNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_remove/userTestId"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void removeProjectNotAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_remove/userTestId"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "adminTest", password = "pass", roles = {"ADMINISTRATOR"})
    public void removeProject() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/user_remove/userTestId"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admin/all_users"));
        Assert.assertNotNull(testUser);
        @Nullable final User user = userService.findById(testUser.getId());
        Assert.assertNull(user);
    }

}
