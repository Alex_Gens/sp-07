package ru.kazakov.iteco.marshalling;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;
import java.util.Date;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class MarshallingTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @Nullable
    private User testUser;

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            user.setDateCreate(new Date());
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @Test
    public void projectTest() throws Exception {
        @NotNull final Project project = new Project();
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        project.setDateStart(new Date(System.currentTimeMillis() - 100000000L));
        project.setDateCreate(new Date(System.currentTimeMillis() + 100000000L));
        project.setStatus(Status.IN_PROGRESS);
        project.setDescription("Some description");
        @Nullable final ProjectDTO dto = domainService.getProjectDTO(project);
        ObjectMapper mapper = new ObjectMapper();
        @NotNull final String projectJson = mapper.writeValueAsString(dto);
        @Nullable final ProjectDTO resultDto = mapper.readValue(projectJson, ProjectDTO.class);
        Assert.assertNotNull(resultDto);
        @Nullable final Project result = domainService.getProjectFromDTO(resultDto);
        Assert.assertNotNull(result);
        Assert.assertEquals(project.getId(), result.getId());
        Assert.assertEquals(project.getName(), result.getName());
        Assert.assertEquals(project.getUser().getId(), result.getUser().getId());
        Assert.assertEquals(project.getDateCreate(), result.getDateCreate());
        Assert.assertEquals(project.getDateStart(), result.getDateStart());
        Assert.assertEquals(project.getDateFinish(), result.getDateFinish());
        Assert.assertEquals(project.getStatus(), result.getStatus());
        Assert.assertEquals(project.getDescription(), result.getDescription());
    }

    @Test
    public void TaskTest() throws Exception {
        @NotNull final Task task = new Task();
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        task.setDateStart(new Date(System.currentTimeMillis() - 100000000L));
        task.setDateCreate(new Date(System.currentTimeMillis() + 100000000L));
        task.setStatus(Status.IN_PROGRESS);
        task.setDescription("Some description");
        @Nullable final TaskDTO dto = domainService.getTaskDTO(task);
        ObjectMapper mapper = new ObjectMapper();
        @NotNull final String projectJson = mapper.writeValueAsString(dto);
        @Nullable final TaskDTO resultDto = mapper.readValue(projectJson, TaskDTO.class);
        Assert.assertNotNull(resultDto);
        @Nullable final Task result = domainService.getTaskFromDTO(resultDto);
        Assert.assertNotNull(result);
        Assert.assertEquals(task.getId(), result.getId());
        Assert.assertEquals(task.getName(), result.getName());
        Assert.assertEquals(task.getUser().getId(), result.getUser().getId());
        Assert.assertEquals(task.getDateCreate(), result.getDateCreate());
        Assert.assertEquals(task.getDateStart(), result.getDateStart());
        Assert.assertEquals(task.getDateFinish(), result.getDateFinish());
        Assert.assertEquals(task.getStatus(), result.getStatus());
        Assert.assertEquals(task.getDescription(), result.getDescription());
    }

    @Test
    public void UserTest() throws Exception {
        @Nullable final UserDTO dto = domainService.getUserDTO(testUser);
        ObjectMapper mapper = new ObjectMapper();
        @NotNull final String userJson = mapper.writeValueAsString(dto);
        @Nullable final UserDTO resultDto = mapper.readValue(userJson, UserDTO.class);
        Assert.assertNotNull(resultDto);
        @Nullable final User result = domainService.getUserFromDTO(resultDto);
        Assert.assertNotNull(result);
        Assert.assertEquals(testUser.getId(), result.getId());
        Assert.assertEquals(testUser.getUsername(), result.getUsername());
        Assert.assertEquals(testUser.getDateCreate(), result.getDateCreate());
    }

}
