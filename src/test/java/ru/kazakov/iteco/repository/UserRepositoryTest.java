package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.model.User;

import java.util.Date;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class UserRepositoryTest {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private User getUserInstance() {
        @NotNull final User user = new User();
        user.setId("userTestId");
        user.setUsername("userTestName");
        user.setDateCreate(new Date());
        return user;
    }

    @Test
    public void save() {
        @Nullable final User user = getUserInstance();
        @Nullable final User beforeSave = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNull(beforeSave);
        userRepository.save(user);
        @Nullable final User afterSave = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNotNull(afterSave);
        userRepository.deleteById(afterSave.getId());
    }

    @Test
    public void findById() {
        @Nullable final User user = getUserInstance();
        userRepository.save(user);
        @Nullable final User wrong = userRepository.findById("wrongId").orElse(null);
        Assert.assertNull(wrong);
        @Nullable final User correct = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNotNull(correct);
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findByUsername() {
        @Nullable final User user = getUserInstance();
        userRepository.save(user);
        @Nullable final User wrong = userRepository.findByUsername("wrongUsername");
        Assert.assertNull(wrong);
        @Nullable final User correct = userRepository.findByUsername(user.getUsername());
        Assert.assertNotNull(correct);
        userRepository.deleteById(user.getId());
    }

    @Test
    public void deleteById() {
        @Nullable final User user = getUserInstance();
        userRepository.save(user);
        @Nullable final User beforeDelete = userRepository.findByUsername(user.getUsername());
        Assert.assertNotNull(beforeDelete);
        userRepository.deleteById(user.getId());
        @Nullable final User afterDelete = userRepository.findByUsername(user.getUsername());
        Assert.assertNull(afterDelete);
    }

}