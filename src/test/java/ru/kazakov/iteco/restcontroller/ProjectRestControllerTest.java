package ru.kazakov.iteco.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import javax.annotation.Resource;
import java.util.Date;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ProjectRestControllerTest {

    @Nullable
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Resource
    private FilterChainProxy springSecurityFilterChain;

    @Nullable
    private User testUser;

    @Before
    public void initMock() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @Test
    public void getAllProjectsNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
               .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void getAllProjects() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
               .andExpect(status().isOk());
    }

    @Test
    public void getProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/projects/projectTestId"))
               .andExpect(status().isUnauthorized());
        projectService.deleteById(project.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void getProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/projects/projectTestId"))
               .andExpect(status().isOk());
        projectService.deleteById(project.getId());
    }

    @Test
    public void addProjectNotAuth() throws Exception {
        @NotNull final ProjectDTO dto = new ProjectDTO();
        Assert.assertNotNull(testUser);
        dto.setId("projectTestId");
        dto.setName("projectTestName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.post("/projects")
               .content(dtoJson)
               .accept(MediaType.APPLICATION_JSON)
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void addProject() throws Exception {
        @NotNull final ProjectDTO dto = new ProjectDTO();
        Assert.assertNotNull(testUser);
        dto.setId("projectTestId");
        dto.setName("projectTestName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.post("/projects")
                    .content(dtoJson)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated());
        projectService.deleteById(dto.getId());
    }

    @Test
    public void deleteProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.delete("/projects/projectTestId"))
               .andExpect(status().isUnauthorized());
        projectService.deleteById(project.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void deleteProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        projectService.save(project);
        mockMvc.perform(MockMvcRequestBuilders.delete("/projects/projectTestId"))
               .andExpect(status().isNoContent());
    }

    @Test
    public void updateProjectNotAuth() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        projectService.save(project);
        @NotNull final ProjectDTO dto = new ProjectDTO();
        Assert.assertNotNull(testUser);
        dto.setId("projectTestId");
        dto.setName("projectUpdatedName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.put("/projects/projectTestId")
                    .content(dtoJson)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isUnauthorized());
        @Nullable final Project updatedProject = projectService.findById(project.getId());
        Assert.assertNotNull(updatedProject);
        Assert.assertNotEquals("projectUpdatedName", updatedProject.getName());
        projectService.deleteById(project.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void updateProject() throws Exception {
        @NotNull final Project project = new Project();
        Assert.assertNotNull(testUser);
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        projectService.save(project);
        @NotNull final ProjectDTO dto = new ProjectDTO();
        dto.setId("projectTestId");
        dto.setName("projectUpdatedName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.put("/projects")
                   .content(dtoJson)
                   .accept(MediaType.APPLICATION_JSON)
                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk());
        @Nullable final Project updatedProject = projectService.findById(project.getId());
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("projectUpdatedName", updatedProject.getName());
        projectService.deleteById(project.getId());
    }

}
