package ru.kazakov.iteco.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;
import javax.annotation.Resource;
import java.util.Date;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TaskRestControllerTest {

    @Nullable
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @Resource
    private FilterChainProxy springSecurityFilterChain;

    @Nullable
    private User testUser;

    @Before
    public void initMock() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @Test
    public void getAllTasksNotAuth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void getAllTasks() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(status().isOk());
    }

    @Test
    public void getTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/taskTestId"))
                .andExpect(status().isUnauthorized());
        taskService.deleteById(task.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void getTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/taskTestId"))
                .andExpect(status().isOk());
        taskService.deleteById(task.getId());
    }

    @Test
    public void addTaskNotAuth() throws Exception {
        @NotNull final TaskDTO dto = new TaskDTO();
        Assert.assertNotNull(testUser);
        dto.setId("taskTestId");
        dto.setName("taskTestName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.post("/tasks")
                .content(dtoJson)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void addTask() throws Exception {
        @NotNull final TaskDTO dto = new TaskDTO();
        Assert.assertNotNull(testUser);
        dto.setId("taskTestId");
        dto.setName("taskTestName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.post("/tasks")
                .content(dtoJson)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        taskService.deleteById(dto.getId());
    }

    @Test
    public void deleteTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.delete("/tasks/taskTestId"))
                .andExpect(status().isUnauthorized());
        taskService.deleteById(task.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void deleteTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        taskService.save(task);
        mockMvc.perform(MockMvcRequestBuilders.delete("/tasks/taskTestId"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateTaskNotAuth() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        taskService.save(task);
        @NotNull final TaskDTO dto = new TaskDTO();
        Assert.assertNotNull(testUser);
        dto.setId("taskTestId");
        dto.setName("taskUpdatedName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.put("/tasks/taskTestId")
                .content(dtoJson)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
        @Nullable final Task updatedTask = taskService.findById(task.getId());
        Assert.assertNotNull(updatedTask);
        Assert.assertNotEquals("taskUpdatedName", updatedTask.getName());
        taskService.deleteById(task.getId());
    }

    @Test
    @WithMockUser(username = "userTest", password = "pass")
    public void updateTask() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertNotNull(testUser);
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        taskService.save(task);
        @NotNull final TaskDTO dto = new TaskDTO();
        dto.setId("taskTestId");
        dto.setName("taskUpdatedName");
        dto.setUserId(testUser.getId());
        dto.setDateCreate(new Date());
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String dtoJson = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.put("/tasks")
                .content(dtoJson)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        @Nullable final Task updatedTask = taskService.findById(task.getId());
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("taskUpdatedName", updatedTask.getName());
        taskService.deleteById(task.getId());
    }
    
}
