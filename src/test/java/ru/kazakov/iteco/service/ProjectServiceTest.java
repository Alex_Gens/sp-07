package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import java.util.Date;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    private User testUser;

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @NotNull
    private Project getProjectInstance() {
        @NotNull final Project project = new Project();
        project.setId("projectTestId");
        project.setName("projectTestName");
        project.setUser(testUser);
        project.setDateCreate(new Date());
        return project;
    }

    @Test
    public void save() throws Exception {
        @NotNull final Project project = getProjectInstance();
        @Nullable final Project beforeSave = projectService.findById(project.getId());
        Assert.assertNull(beforeSave);
        projectService.save(project);
        @Nullable final Project afterSave = projectService.findById(project.getId());
        Assert.assertNotNull(afterSave);
        Assert.assertEquals(project.getId(), afterSave.getId());
    }

    @Test
    public void findById() throws Exception {
        @NotNull final Project project = getProjectInstance();
        projectService.save(project);
        @Nullable final Project found = projectService.findById(project.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(project.getId(), found.getId());
    }

    @Test
    public void findOne() throws Exception {
        @NotNull final Project project = getProjectInstance();
        projectService.save(project);
        @Nullable final Project foundWrong = projectService.findOne(project.getId(), "wrongUserId");
        Assert.assertNull(foundWrong);
        @Nullable final Project found = projectService.findOne(project.getId(), testUser.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(project.getId(), found.getId());
    }

    @Test
    public void existById() throws Exception {
        @NotNull final Project project = getProjectInstance();
        projectService.save(project);
        final boolean isNotExist = projectService.existsById("wrongId");
        Assert.assertFalse(isNotExist);
        final boolean isExist = projectService.existsById(project.getId());
        Assert.assertTrue(isExist);
    }

    @Test
    public void findAllByUserIdOrderByDateCreate() throws Exception {
        @NotNull final Project project = getProjectInstance();
        @NotNull final Project secondProject = getProjectInstance();
        secondProject.setId("secondId");
        secondProject.setName("secondProject");
        projectService.save(project);
        projectService.save(secondProject);
        @NotNull final List<Project> empty = projectService.findAllByUserIdOrderByDateCreate("wrongUserId");
        Assert.assertEquals(0, empty.size());
        @NotNull final List<Project> projects = projectService.findAllByUserIdOrderByDateCreate(testUser.getId());
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void deleteById() throws Exception {
        @NotNull final Project project = getProjectInstance();
        projectService.save(project);
        @Nullable final Project beforeDelete = projectService.findById(project.getId());
        Assert.assertNotNull(beforeDelete);
        projectService.deleteById(project.getId());
        @Nullable final Project afterDelete = projectService.findById(project.getId());
        Assert.assertNull(afterDelete);
    }

    @Test
    public void delete() throws Exception {
        @NotNull final Project project = getProjectInstance();
        projectService.save(project);
        @Nullable final Project beforeDelete = projectService.findById(project.getId());
        Assert.assertNotNull(beforeDelete);
        projectService.delete(project);
        @Nullable final Project afterDelete = projectService.findById(project.getId());
        Assert.assertNull(afterDelete);
    }

}
