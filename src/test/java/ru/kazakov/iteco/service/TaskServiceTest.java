package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;
import java.util.Date;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    private User testUser;

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @NotNull
    private Task getTaskInstance() {
        @NotNull final Task task = new Task();
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        return task;
    }

    @Test
    public void save() throws Exception {
        @NotNull final Task task = getTaskInstance();
        @Nullable final Task beforeSave = taskService.findById(task.getId());
        Assert.assertNull(beforeSave);
        taskService.save(task);
        @Nullable final Task afterSave = taskService.findById(task.getId());
        Assert.assertNotNull(afterSave);
        Assert.assertEquals(task.getId(), afterSave.getId());
    }

    @Test
    public void findById() throws Exception {
        @NotNull final Task task = getTaskInstance();
        taskService.save(task);
        @Nullable final Task found = taskService.findById(task.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(task.getId(), found.getId());
    }

    @Test
    public void findOne() throws Exception {
        @NotNull final Task task = getTaskInstance();
        taskService.save(task);
        @Nullable final Task foundWrong = taskService.findOne(task.getId(), "wrongUserId");
        Assert.assertNull(foundWrong);
        @Nullable final Task found = taskService.findOne(task.getId(), testUser.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(task.getId(), found.getId());
    }

    @Test
    public void findAllByUserIdOrderByDateCreate() throws Exception {
        @NotNull final Task task = getTaskInstance();
        @NotNull final Task secondTask = getTaskInstance();
        secondTask.setId("secondId");
        secondTask.setName("secondTask");
        taskService.save(task);
        taskService.save(secondTask);
        @NotNull final List<Task> empty = taskService.findAllByUserIdOrderByDateCreate("wrongUserId");
        Assert.assertEquals(0, empty.size());
        @NotNull final List<Task> tasks = taskService.findAllByUserIdOrderByDateCreate(testUser.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void deleteById() throws Exception {
        @NotNull final Task task = getTaskInstance();
        taskService.save(task);
        @Nullable final Task beforeDelete = taskService.findById(task.getId());
        Assert.assertNotNull(beforeDelete);
        taskService.deleteById(task.getId());
        @Nullable final Task afterDelete = taskService.findById(task.getId());
        Assert.assertNull(afterDelete);
    }

    @Test
    public void delete() throws Exception {
        @NotNull final Task task = getTaskInstance();
        taskService.save(task);
        @Nullable final Task beforeDelete = taskService.findById(task.getId());
        Assert.assertNotNull(beforeDelete);
        taskService.delete(task);
        @Nullable final Task afterDelete = taskService.findById(task.getId());
        Assert.assertNull(afterDelete);
    }

}
