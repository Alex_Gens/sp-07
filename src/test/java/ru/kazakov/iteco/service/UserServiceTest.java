package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.model.User;
import java.util.Date;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    private User getUserInstance() {
        @NotNull final User user = new User();
        user.setId("userTestId");
        user.setUsername("userTestName");
        user.setDateCreate(new Date());
        return user;
    }

    @Test
    public void create() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = "test";
        @Nullable final User beforeCreate = userService.findByUsername("test");
        Assert.assertNull(beforeCreate);
        userService.create(login, password);
        @Nullable final User afterCreate = userService.findByUsername("test");
        Assert.assertNotNull(afterCreate);
        userService.deleteById(afterCreate.getId());
    }

    @Test
    public void save() throws Exception {
        @Nullable final User user = getUserInstance();
        @Nullable final User beforeSave = userService.findById(user.getId());
        Assert.assertNull(beforeSave);
        userService.save(user);
        @Nullable final User afterSave = userService.findById(user.getId());
        Assert.assertNotNull(afterSave);
        userService.deleteById(afterSave.getId());
    }

    @Test
    public void findById() throws Exception {
        @Nullable final User user = getUserInstance();
        userService.save(user);
        @Nullable final User wrong = userService.findById("wrongId");
        Assert.assertNull(wrong);
        @Nullable final User correct = userService.findById(user.getId());
        Assert.assertNotNull(correct);
        userService.deleteById(user.getId());
    }

    @Test
    public void findByUsername() throws Exception {
        @Nullable final User user = getUserInstance();
        userService.save(user);
        @Nullable final User wrong = userService.findByUsername("wrongUsername");
        Assert.assertNull(wrong);
        @Nullable final User correct = userService.findByUsername(user.getUsername());
        Assert.assertNotNull(correct);
        userService.deleteById(user.getId());
    }

    @Test
    public void deleteById() throws Exception {
        @Nullable final User user = getUserInstance();
        userService.save(user);
        @Nullable final User beforeDelete = userService.findByUsername(user.getUsername());
        Assert.assertNotNull(beforeDelete);
        userService.deleteById(user.getId());
        @Nullable final User afterDelete = userService.findByUsername(user.getUsername());
        Assert.assertNull(afterDelete);
    }

}
